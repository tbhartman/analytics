analytics
=========

*a simple page hit counter*

What it is
----------

`analytics` is a simple self-hosted web server which allows
simple anonymous analytics for your website.  This is an alternative
to other analytics tools which collect much more information
on page views.  `analytics` does not collect IP address, browser
brand, etc.  It only collects:

  * URL of the hit
  * timestamp of the hit

However, stored data strips the time from the timestamp so that
hits are stored only by date.

`analytics` provides a simple view of the data available from the
web interface.

Usage
-----

### Backend

`go get gitlab.com/tbhartman/analytics`

(A compiler is needed due to dependency on
[sqlite3](https://github.com/mattn/go-sqlite3), and, thus, the get
will take some time.)

Run `analytics new <path_to_config>` to generate a configuration file
at `path_to_config`.  Example is:

```yaml
# timezone in which to display analytics times
# strings passed to golang's time.LoadLocation
timezone: UTC

# a secret key for viewing analytics data
frontendkey: 9da0fbd5-8992-43a6-8f62-cdb23736df7f

# path to sqlite3 database file (will be created if not exists)
databasepath: testdb.sqlite3

# host on which to serve
serve:
  host: analytics.example.com
  port: 443

# page url hosts from which to accept hits
# if the Origin header does not match, the hit is rejected
# used as CORS response
hosts:
  - www.example.com
  - another.example.com

tlspublicpath: path_to_fullchain.pem
tlsprivatepath: path_to_privkey.pem

```

Run `analytics <path_to_config>` to start the server.

### Frontend

To process a page view, `POST` to `/hit` on your analytics server, such as:

```javascript
var hit = JSON.stringify(
    {
        "url": document.URL
    });
var xhttp = new XMLHttpRequest();
xhttp.open("POST", "https://yourserver.example.com/hit", true);
xhttp.setRequestHeader("Content-type", "application/json");
xhttp.send(hit);
```

To view current data, turn your browser to `<host>/<key>/list`, for example
(if using the above config):

    https://analytics.example.com/9da0fbd5-8992-43a6-8f62-cdb23736df7f/list

Which currently shows something like:

```
  Uptime              : 1d2h3m4s                  
  Current time        : 2021-01-01 00:00 MST  
  Last hit            : 2020-12-31 23:00 MST  
  Session server hits :                    2  
  Session page hits   :                    1  
  Total page hits     :                    3  


+-------------------+-----------+
|       PAGE        | HIT COUNT |
+-------------------+-----------+
| www.example.com/  |         2 |
+-------------------+-----------+
| www.example.com/a |         1 |
+-------------------+-----------+
```
