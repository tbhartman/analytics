package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/olekukonko/tablewriter"
)

// HitHandler is an http.Handler that accepts the analytics REST API.
type HitHandler interface {
	http.Handler
	io.Closer
}

type hitData struct {
	URL       string    `json:"url"`
	Timestamp time.Time `json:"timestamp"`
}
type serverStats struct {
	Uptime            string
	CurrentTime       string
	LastHit           string
	SessionServerHits int64
	SessionPageHits   int64
	TotalPageHits     int64
}
type listData struct {
	Stats serverStats
	Hits  []struct {
		Page  string
		Count int64
	}
}

type handler struct {
	store          Store
	hiddenKey      string
	hitsToStore    chan hitData
	doneProcessing sync.WaitGroup
}

func (h *handler) Close() error {
	close(h.hitsToStore)
	h.doneProcessing.Wait()
	return nil
}
func (h *handler) ProcessHits() {
	h.doneProcessing.Add(1)
	go func() {
		defer h.doneProcessing.Done()
		for hit := range h.hitsToStore {
			h.store.Hit(hit.URL, hit.Timestamp)
		}
	}()
}

func (h *handler) checkCORS(res http.ResponseWriter, req *http.Request) bool {
	origin, err := url.Parse(req.Header.Get("Origin"))
	if err != nil || !h.store.ServesHost(origin.Host) {
		res.WriteHeader(http.StatusForbidden)
		return false
	}
	// https://stackoverflow.com/a/7605119
	res.Header().Set("Access-Control-Allow-Origin", "https://"+origin.Host)
	res.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	res.Header().Set("Access-Control-Max-Age", " 1000")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With")
	return true
}
func (h *handler) getListing() (ret listData) {
	timeStr := "2006-01-02 15:04 MST"
	meta := h.store.Meta()
	ret.Stats.CurrentTime = time.Now().In(timezone).Format(timeStr)
	ret.Stats.Uptime = time.Since(startTime).Truncate(time.Second).String()
	ret.Stats.LastHit = meta.LastHit.In(timezone).Format(timeStr)
	ret.Stats.TotalPageHits = meta.Hits
	ret.Stats.SessionPageHits = sessionHits
	ret.Stats.SessionServerHits = sessionServerHits

	listing := h.store.List()
	pages := make([]string, 0, len(listing))
	for k := range listing {
		pages = append(pages, k)
	}
	sort.Strings(pages)

	ret.Hits = make([]struct {
		Page  string
		Count int64
	}, len(pages))
	for i, k := range pages {
		ret.Hits[i].Count = listing[k]
		ret.Hits[i].Page = k
	}
	return
}
func (h *handler) ServeHTTPHit(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodOptions:
		if !h.checkCORS(res, req) {
			res.WriteHeader(http.StatusForbidden)
			return
		}
		res.WriteHeader(http.StatusOK)
	case http.MethodPost:
		if !h.checkCORS(res, req) {
			res.WriteHeader(http.StatusForbidden)
			return
		}
		var hit hitData
		d := json.NewDecoder(req.Body)
		err := d.Decode(&hit)
		if err != nil {
			if debug {
				fmt.Println("Bad request")
			}
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		res.WriteHeader(http.StatusOK)
		hit.Timestamp = time.Now()
		if debug {
			fmt.Println("Got Request:")
			fmt.Println(hit)
		}
		sessionHits++
		h.hitsToStore <- hit
	default:
		res.WriteHeader(http.StatusForbidden)
	}
}
func (h *handler) ServeHTTPList(res http.ResponseWriter, req *http.Request) {
	listing := h.getListing()
	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "text/plain; charset=utf-8")

	metaTable := tablewriter.NewWriter(res)
	metaTable.AppendBulk([][]string{
		{"Uptime", listing.Stats.Uptime},
		{"Current time", listing.Stats.CurrentTime},
		{"Last hit", listing.Stats.LastHit},
		{"Session server hits", strconv.Itoa(int(listing.Stats.SessionServerHits))},
		{"Session page hits", strconv.Itoa(int(listing.Stats.SessionPageHits))},
		{"Total page hits", strconv.Itoa(int(listing.Stats.TotalPageHits))},
	})
	metaTable.SetBorder(false)
	metaTable.SetColumnSeparator(":")
	metaTable.Render()
	res.Write([]byte("\n\n"))
	table := tablewriter.NewWriter(res)
	table.SetHeader([]string{"Page", "Hit Count"})

	for _, row := range listing.Hits {
		table.Append([]string{row.Page, strconv.Itoa(int(row.Count))})
	}
	table.Render()
}
func (h *handler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	sessionServerHits++
	switch {
	case req.URL.Path == "/hit":
		h.ServeHTTPHit(res, req)
		return
	case len(h.hiddenKey) > 0 && strings.HasPrefix(req.URL.Path, "/"+h.hiddenKey):
		switch {
		case strings.HasSuffix(req.URL.Path, "/list"):
			h.ServeHTTPList(res, req)
			return
		case strings.HasSuffix(req.URL.Path, "/kill"):
			res.WriteHeader(http.StatusNotImplemented)
		default:
			res.WriteHeader(http.StatusNotFound)
		}
	default:
		res.WriteHeader(http.StatusForbidden)
	}
	return
}

// NewHitHandler returns a HitHandler with the hit processing started.
func NewHitHandler(store Store, hitBufferLength int, hiddenKey string) HitHandler {
	h := &handler{
		store:       store,
		hiddenKey:   hiddenKey,
		hitsToStore: make(chan hitData, hitBufferLength),
	}
	h.ProcessHits()
	return h
}
