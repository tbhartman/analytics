module gitlab.com/tbhartman/analytics

go 1.15

require (
	github.com/google/uuid v1.2.0
	github.com/lithammer/dedent v1.1.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/olekukonko/tablewriter v0.0.4
	github.com/sergi/go-diff v1.1.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.4.0
)
