package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/google/uuid"
	"gopkg.in/yaml.v2"
)

var debug bool
var startTime time.Time
var sessionHits int64
var sessionServerHits int64
var timezone *time.Location = time.UTC

func init() {
	startTime = time.Now()
}

// Config stores all configuration
type Config struct {
	DatabasePath string
	Serve        struct {
		Port int
		Host string
	}
	Hosts          []string
	Debug          bool
	FrontEndKey    string
	TLSPublicPath  string
	TLSPrivatePath string
	TimeZone       string
}

func generateConfig(path string) {
	f, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	e := yaml.NewEncoder(f)
	loc, _ := time.LoadLocation("America/New_York")
	err = e.Encode(Config{
		DatabasePath: "pathToDatabase.sqlite3",
		Serve: struct {
			Port int
			Host string
		}{
			Host: "",
			Port: 8080,
		},
		Hosts:          []string{"www.example.com", "www2.example.com"},
		Debug:          false,
		FrontEndKey:    uuid.New().String(),
		TLSPublicPath:  "path-to-fullchain.pem",
		TLSPrivatePath: "path-to-key.pem",
		TimeZone:       loc.String(),
	})
	if err != nil {
		log.Println("Error writing new config")
		log.Fatal(err)
	}
	return
}

func readArgs() string {
	var configPath string
	// check for bad args
	if len(os.Args) > 1 {
		configPath = os.Args[1]
	}
	var invalid bool
	switch {
	case configPath == "":
		invalid = true
	case configPath == "help":
		invalid = false
	case configPath == "new" && len(os.Args) == 3:
		invalid = false
	case len(os.Args) == 2:
		invalid = false
	default:
		invalid = true
	}
	if invalid {
		fmt.Println("Invalid arguments")
	}
	if configPath == "help" || configPath == "" {
		fmt.Println(`
analytics v0.0
analytics is a simple web app for gathering page view data

Usage:

	analytics <configPath>
	analytics new <configPath>

where configPath points to a valid configuration file or,
if new is given, is a path where a new config can be written.`)
		if invalid {
			os.Exit(1)
		} else {
			os.Exit(0)
		}
	}
	return configPath
}

func getConfig(path string) Config {
	f, err := os.Open(path)
	if err != nil {
		log.Printf("Invalid config: %s\n", path)
		log.Fatal(err)
	}
	var config Config
	func() {
		defer f.Close()
		d := yaml.NewDecoder(f)
		err = d.Decode(&config)
	}()
	if err != nil {
		log.Println("Configuration error")
		log.Fatal(err)
	}

	timezone, err = time.LoadLocation(config.TimeZone)
	if err != nil {
		log.Print("Invalid config TimeZone")
		log.Fatal(err)
	}
	return config
}

func main() {
	var configPath string = readArgs()

	if configPath == "new" {
		generateConfig(os.Args[2])
		return
	}

	config := getConfig(configPath)
	fmt.Printf("configuration read from %s\n", configPath)

	debug = config.Debug

	s, err := NewStore(config.DatabasePath, config.Hosts)
	if err != nil {
		log.Print("Failed to start database")
		log.Fatal(err)
	}
	defer s.Close()
	h := NewHitHandler(s, 10000, config.FrontEndKey)
	defer h.Close()

	// handle `/hit` route
	http.Handle("/", h)

	// run server on port "9000"
	address := fmt.Sprintf("%s:%d", config.Serve.Host, config.Serve.Port)
	fmt.Printf("listening on %s\n", address)
	err = http.ListenAndServeTLS(address, config.TLSPublicPath, config.TLSPrivatePath, nil)
	check(err)
}
