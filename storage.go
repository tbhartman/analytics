package main

import (
	"database/sql"
	"io"
	"log"
	"net/url"
	"strings"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

// Store exposes interfaces to the database
type Store interface {
	io.Closer
	Hit(fullpath string, timestamp time.Time) error
	Init(hostnames []string) error
	List() map[string]int64
	Meta() DatabaseMeta
	ServesHost(hostname string) bool
}

type storeStruct struct {
	path       string
	connection *sql.DB
	hosts      map[string]int64
	pages      map[string]int64
	dates      map[string]int64
	lock       sync.Mutex
}

func check(e error) {
	if e != nil {
		log.Panic(e)
	}
}

// DatabaseMeta is the metadata from a database.
type DatabaseMeta struct {
	Hits    int64
	LastHit time.Time
}

func (s *storeStruct) Meta() DatabaseMeta {
	s.lock.Lock()
	defer s.lock.Unlock()
	var id int64
	var totalHits int64
	var lastHitStr string
	rows, err := s.connection.Query(`SELECT * FROM meta`)
	if err != nil {
		check(err)
		return DatabaseMeta{}
	}
	defer rows.Close()
	rows.Next()
	err = rows.Scan(&id, &totalHits, &lastHitStr)
	check(err)
	lastHit, _ := time.Parse(time.RFC3339, lastHitStr)
	return DatabaseMeta{
		Hits:    totalHits,
		LastHit: lastHit,
	}
}
func (s *storeStruct) ServesHost(hostname string) (ok bool) {
	_, ok = s.hosts[hostname]
	return
}
func (s *storeStruct) List() map[string]int64 {
	s.lock.Lock()
	defer s.lock.Unlock()
	rows, err := s.connection.Query(`SELECT * FROM pageViews`)
	if err != nil {
		return make(map[string]int64)
	}
	var host string
	var page string
	var count int64
	ret := make(map[string]int64)
	for rows.Next() {
		rows.Scan(&host, &page, &count)
		ret[host+page] = count
	}
	return ret
}

func (s *storeStruct) Hit(fullpath string, timestamp time.Time) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	var trxClose sync.Once
	trx, err := s.connection.Begin()
	if err != nil {
		return err
	}
	defer trxClose.Do(func() {
		trx.Rollback()
	})

	dateStr := timestamp.Format("2006-01-02")
	timeStr := timestamp.Format(time.RFC3339)

	// get date ID
	var dateID int64
	{
		var err error
		var ok bool
		var rows *sql.Rows
		dateID, ok = s.dates[dateStr]
		if !ok {
			rows, err = trx.Query("SELECT id FROM dates WHERE date = $1", dateStr)
			check(err)
			if rows.Next() {
				err = rows.Scan(&dateID)
				rows.Close()
				check(err)
			} else {
				result, err := trx.Exec(
					"INSERT INTO dates (date) VALUES ($1)",
					dateStr,
				)
				check(err)
				dateID, err = result.LastInsertId()
				check(err)
			}
		}
	}

	// parse url
	var pageURL *url.URL
	pageURL, err = url.Parse(fullpath)
	hostname := pageURL.Host
	path := "/" + strings.TrimPrefix(pageURL.Path, "/")
	page := hostname + path

	// get page ID
	var pageID int64
	{
		var err error
		var ok bool
		var rows *sql.Rows
		pageID, ok = s.pages[page]
		if !ok {
			var hostID int64
			hostID, ok = s.hosts[hostname]
			if !ok {
				// this host is not recogized; drop
				return trx.Rollback()
			}
			rows, err = trx.Query(
				"SELECT id FROM pages WHERE hostId = $1 AND path = $2",
				hostID,
				path,
			)
			check(err)
			if rows.Next() {
				err = rows.Scan(&pageID)
				rows.Close()
				check(err)
			} else {
				result, err := trx.Exec(
					"INSERT INTO pages (hostId, path) VALUES ($1, $2)",
					hostID,
					path,
				)
				check(err)
				pageID, err = result.LastInsertId()
				check(err)
			}
		}
	}

	// at this point, assured that pageId and dateId exist

	result, _ := trx.Exec(
		`UPDATE hits
			SET count = count + 1
			WHERE pageId = $1 AND dateId = $2`,
		pageID,
		dateID,
	)
	if rowsAffected, _ := result.RowsAffected(); rowsAffected == 0 {
		// this date/page combo might not exist
		result, err = trx.Exec(
			`INSERT INTO hits (pageId, dateId, count) VALUES ($1, $2, 1)`,
			pageID,
			dateID,
		)
		check(err)
		if rowsAffected, _ = result.RowsAffected(); rowsAffected != 1 {
			log.Fatalln("Error INSERT INTO hits")
		}

	}
	trx.Exec(
		`update meta
		set totalHits = totalHits + 1,
		    lastHit = $1`,
		timeStr,
	)
	var done bool
	trxClose.Do(func() {
		done = true
		err = trx.Commit()
	})
	if !done {
		log.Fatal("commit not called")
	}
	if err != nil {
		log.Print(err)
		panic(err)
	}
	return nil
}

func (s *storeStruct) Init(hostnames []string) (err error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.hosts = make(map[string]int64)
	s.pages = make(map[string]int64)
	s.dates = make(map[string]int64)
	s.connection, err = sql.Open("sqlite3", s.path)
	if err != nil {
		return
	}

	trx, err := s.connection.Begin()
	check(err)
	var endTrx sync.Once
	defer endTrx.Do(func() { trx.Rollback() })

	var statement *sql.Stmt
	for _, statementText := range []string{
		`CREATE TABLE IF NOT EXISTS
		meta (
			id INTEGER PRIMARY KEY,
			totalHits INTEGER,
			lastHit TEXT,
			constraint CK_meta_Locked CHECK (id=1)
		)`,
		`CREATE TABLE IF NOT EXISTS
		hosts (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			hostname TEXT);`,
		`CREATE TABLE IF NOT EXISTS
		pages (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			hostId INTEGER,
			path TEXT,
			FOREIGN KEY(hostId) REFERENCES hosts(id));`,
		`CREATE TABLE IF NOT EXISTS
		dates (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			date TEXT);`,
		`CREATE TABLE IF NOT EXISTS
		hits (
			pageId INTEGER,
			dateId INTEGER,
			count INTEGER,
			FOREIGN KEY(pageId) REFERENCES pages(id),
			FOREIGN KEY(dateId) REFERENCES dates(id),
			PRIMARY KEY (pageId, dateId));`,
		`INSERT INTO meta SELECT 1, 0, "" WHERE NOT EXISTS (SELECT * FROM meta)`,
		`CREATE VIEW IF NOT EXISTS pageViews
		AS 
		SELECT hostname, path, SUM(count)
		FROM hits h
			INNER JOIN pages p ON p.id = h.pageId
			INNER JOIN hosts t ON p.hostId = t.id
			INNER JOIN dates d ON d.id = h.dateId
		GROUP BY hostname, path
		ORDER BY hostname, path`,
	} {
		statement, err = s.connection.Prepare(statementText)
		check(err)
		_, err = statement.Exec()
		check(err)
	}

	// fill in host ids
	var rows *sql.Rows
	var hostID int64
	var hostname string
	rows, err = s.connection.Query(`SELECT * FROM hosts`)
	for rows.Next() {
		err = rows.Scan(&hostID, &hostname)
		check(err)
		s.hosts[hostname] = hostID
	}
	for _, hostname = range hostnames {
		if _, found := s.hosts[hostname]; !found {
			result, err := s.connection.Exec(`INSERT INTO hosts (hostname) VALUES ($1)`, hostname)
			check(err)
			id, err := result.LastInsertId()
			check(err)
			s.hosts[hostname] = id
		}
	}
	endTrx.Do(func() { err = trx.Commit() })
	check(err)

	return
}
func (s *storeStruct) Close() error {
	s.connection.Close()
	return nil
}

// NewStore creates a new database
func NewStore(path string, hostnames []string) (ret Store, err error) {
	ret = &storeStruct{
		path: path,
	}
	err = ret.Init(hostnames)
	return
}
